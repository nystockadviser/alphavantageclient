package com.nyounes.alphavantageclient.config.model;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "co.alphavantage.api")
public class AlphaVantageAPIConfiguration {
    private String apiKey;
    private String url;
    private String searchSymbolUrl;
}
