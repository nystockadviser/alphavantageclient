package com.nyounes.alphavantageclient.search.controller;

import com.nyounes.alphavantageclient.search.model.SearchResult;
import com.nyounes.alphavantageclient.search.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SearchController {
    @Autowired
    private SearchService searchService;

    @GetMapping("/search/{keyword}")
    public List<SearchResult> getSymbolsOrCompaniesByKeyword(@PathVariable String keyword) {
        return searchService.getSymbolsOrCompaniesByKeyword(keyword);
    }
}
