package com.nyounes.alphavantageclient.search.client.impl;

import com.nyounes.alphavantageclient.config.model.AlphaVantageAPIConfiguration;
import com.nyounes.alphavantageclient.search.client.SearchClient;
import com.nyounes.alphavantageclient.search.model.SearchMathes;
import com.nyounes.alphavantageclient.search.model.SearchResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import java.util.List;
import java.util.Map;

@Component
public class SearchClientImpl implements SearchClient {
    @Autowired
    private AlphaVantageAPIConfiguration alphaVantageAPIConfiguration;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public List<SearchResult> getSymbolsOrCompaniesByKeyword(String keyword) {
        return restTemplate
                .getForObject(alphaVantageAPIConfiguration.getSearchSymbolUrl(), SearchMathes.class, populateParams(keyword))
                .getBestMatches();
    }

    private Map<String, String> populateParams(String keyword) {
        return Map.of(
                "keyword", keyword,
                "apiKey", alphaVantageAPIConfiguration.getApiKey()
        );
    }
}
