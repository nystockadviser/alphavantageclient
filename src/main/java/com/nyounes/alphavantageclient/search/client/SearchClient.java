package com.nyounes.alphavantageclient.search.client;

import com.nyounes.alphavantageclient.search.model.SearchResult;

import java.util.List;

public interface SearchClient {
    List<SearchResult> getSymbolsOrCompaniesByKeyword(String keyword);
}
