package com.nyounes.alphavantageclient.search.service;

import com.nyounes.alphavantageclient.search.model.SearchResult;

import java.util.List;

public interface SearchService {
    List<SearchResult> getSymbolsOrCompaniesByKeyword(String keyword);
}
