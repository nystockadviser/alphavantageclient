package com.nyounes.alphavantageclient.search.service.impl;

import com.nyounes.alphavantageclient.search.client.SearchClient;
import com.nyounes.alphavantageclient.search.model.SearchResult;
import com.nyounes.alphavantageclient.search.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SearchServiceImpl implements SearchService {
    @Autowired
    private SearchClient searchClient;

    public List<SearchResult> getSymbolsOrCompaniesByKeyword(String keyword) {
        return searchClient.getSymbolsOrCompaniesByKeyword(keyword);
    }
}
