package com.nyounes.alphavantageclient.search.model;

import lombok.Data;

import java.util.List;

@Data
public class SearchMathes {
    private List<SearchResult> bestMatches;
}
