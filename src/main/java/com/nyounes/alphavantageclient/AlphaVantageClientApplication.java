package com.nyounes.alphavantageclient;

import com.nyounes.alphavantageclient.config.model.AlphaVantageAPIConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(AlphaVantageAPIConfiguration.class)
public class AlphaVantageClientApplication {
    public static void main(String[] args) {
        SpringApplication.run(AlphaVantageClientApplication.class, args);
    }
}
